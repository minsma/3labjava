package lab3smalinskas;

import laborai.studijosktu.BstSetKTU;
import laborai.studijosktu.SetADT;

public class TelefonuApskaita {
    
    public static SetADT<String> telefonuModeliai(Telefonas[] telefonai){
        SetADT<Telefonas> tel = new BstSetKTU<>(Telefonas.pagalModeli);
        SetADT<String> kart = new BstSetKTU<>();
        
        for(Telefonas t : telefonai){
            int sizeBefore = tel.size();
            tel.add(t);
            
            if(sizeBefore == tel.size())
                kart.add(t.getModelis());
        }
        
        return kart;
    }
}
