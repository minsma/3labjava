package lab3smalinskas;

import laborai.studijosktu.Ks;
import laborai.studijosktu.AvlSetKTUx;
import laborai.studijosktu.SortedSetADTx;
import laborai.studijosktu.SetADT;
import laborai.studijosktu.BstSetKTUx;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Locale;
import laborai.studijosktu.AvlSetKTU;
import laborai.studijosktu.BstSetKTU;

public class TelefonuTestai {
    
    static Telefonas[] telefonuBaze;
    static SortedSetADTx<Telefonas> tSerija = new BstSetKTUx(new Telefonas(), Telefonas.pagalKaina);
    
    public static void main(String[] args) throws CloneNotSupportedException {
        Locale.setDefault(Locale.US);
        aibesTestas();
    }
    
    static SortedSetADTx generuotiAibes(int kiekis, int generN){
        telefonuBaze = new Telefonas[generN];
        
        for(int i = 0; i < generN; i++)
            telefonuBaze[i] = new Telefonas.Builder().buildRandom();
        
        Collections.shuffle(Arrays.asList(telefonuBaze));
        tSerija.clear();
        
        for(int i = 0; i < kiekis; i++)
            tSerija.add(telefonuBaze[i]);
        
        return tSerija;
    }
    
    public static void aibesTestas() throws CloneNotSupportedException {
        Telefonas telefonas = new Telefonas("LG", "G5", 2016, 450.0);
        Telefonas telefonas2 = new Telefonas.Builder()
                .gamintojas("Iphone")
                .modelis("X")
                .isleidMetai(2017)
                .kaina(1200.0)
                .build();
        Telefonas telefonas3 = new Telefonas.Builder().buildRandom();
        Telefonas telefonas4 = new Telefonas("Iphone 5s 2013 500.0");
        Telefonas telefonas5 = new Telefonas("Iphone 6 2014 600.0");
        Telefonas telefonas6 = new Telefonas("Iphone 6s 2015 650.0");
        Telefonas telefonas7 = new Telefonas("Iphone 7 2016 660.0");
        
        Telefonas[] telefonuMasyvas = {telefonas3, telefonas7, telefonas5,
            telefonas6, telefonas4, telefonas3};
        
        Ks.oun("Telefonu aibe: ");
        SortedSetADTx<Telefonas> telefonuAibe = new BstSetKTUx(new Telefonas());
        
        for(Telefonas tel : telefonuMasyvas){
            telefonuAibe.add(tel);
            Ks.oun("Aibe papildoma: " + tel + ". Jos dydis " + telefonuAibe.size());
        }
        
        Ks.oun("");
        Ks.oun(telefonuAibe.toVisualizedString(""));
        
        Ks.oun("Telefonu medzio aukstis: ");
        
        SortedSetADTx<Telefonas> telefonuAibeKopija
                = (SortedSetADTx<Telefonas>) telefonuAibe.clone();
        
        telefonuAibeKopija.add(telefonas);
        telefonuAibeKopija.add(telefonas2);
        telefonuAibeKopija.add(telefonas3);
        telefonuAibeKopija.add(telefonas4);
        
        Ks.oun("Papildyta telefonu aibes kopija:");
        Ks.oun(telefonuAibeKopija.toVisualizedString(""));
        
        Ks.oun("Ar elementai egzistuoja aibeje?");
        
        for(Telefonas tel : telefonuMasyvas)
            Ks.oun(tel + ": " + telefonuAibe.contains(tel));
        
        Ks.oun(telefonas2 + ": " + telefonuAibe.contains(telefonas2));
        Ks.oun(telefonas3 + ": " + telefonuAibe.contains(telefonas3));
        Ks.oun(telefonas4 + ": " + telefonuAibe.contains(telefonas4));
        Ks.oun("");
        
        Ks.oun("Ar elementai egzistuoja aibes kopijoje?");

        for(Telefonas tel : telefonuMasyvas)
            Ks.oun(tel + ": " + telefonuAibeKopija.contains(tel));
        
        Ks.oun("");
        Ks.oun("Elementu salinimas is kopijos. Aibes dydis pries salinima " + 
                telefonuAibeKopija.size());
        
        for(Telefonas tel : new Telefonas[] {telefonas2, telefonas, 
            telefonas7, telefonas5, telefonas3, telefonas4, telefonas6}){
            telefonuAibeKopija.remove(tel);
            Ks.oun("Is telefonu aibes kopijos pasalinama: " + tel + ". Jos dydis " 
                    + telefonuAibeKopija.size());
        }
        
        Ks.oun("");
        Ks.oun("Telefonu aibe su iteratoriumi:");
        Ks.oun("");
        
        for(Telefonas tel : telefonuAibe)
            Ks.oun(tel);
        
        Ks.oun("");
        Ks.oun("Telefonu aibe AVL-medyje:");
        
        SortedSetADTx<Telefonas> telefonuAibe2 = new AvlSetKTUx(new Telefonas());
        
        for(Telefonas tel : telefonuMasyvas)
            telefonuAibe2.add(tel);
        
        Ks.oun(telefonuAibe2.toVisualizedString(""));
        
        Ks.oun("Telefonu aibe su iteratoriu:");
        Ks.oun("");
        
        for(Telefonas tel : telefonuAibe2)
            Ks.oun(tel);
        
        Ks.oun("");
        Ks.oun("Telefonu aibe su atvirkstiniu iteratoriumi:");
        Ks.oun("");
        
        Iterator iter = telefonuAibe2.descendingIterator();
        
        while(iter.hasNext())
            Ks.oun(iter.next());
        
        Ks.oun("");
        Ks.oun("Telefonu aibes toString() metodas:");
        Ks.oun(telefonuAibe2);
        
        telefonuAibe.clear();
        telefonuAibe2.clear();
        
        Ks.oun("");
        Ks.oun("Telefonu aibe DP-medyje:");
        telefonuAibe.load("Duomenys\\data.txt");
        Ks.ounn(telefonuAibe.toVisualizedString(""));
        Ks.oun("Issiaiskinkite, kodel medis augo tik i viena puse");
        
        Ks.oun("");
        Ks.oun("Telefonu aibe AVL-medyje");
        telefonuAibe2.load("duomenys\\data.txt");
        Ks.ounn(telefonuAibe2.toVisualizedString(""));
        
        SetADT<String> telefonuAibe3 = TelefonuApskaita.telefonuModeliai(telefonuMasyvas);
        
        Ks.oun("Pasikartojantys telefonu modeliai:\n" + telefonuAibe3.toString());
    
        BstSetKTU<Telefonas> aibe = new BstSetKTU<Telefonas>();
        aibe.add(new Telefonas.Builder().buildRandom());
        aibe.add(new Telefonas.Builder().buildRandom());
//        aibe.add(new Telefonas.Builder().buildRandom());
//        aibe.add(new Telefonas.Builder().buildRandom());
//        aibe.add(new Telefonas.Builder().buildRandom());
//        aibe.add(new Telefonas.Builder().buildRandom());
//        aibe.add(new Telefonas.Builder().buildRandom());
//        aibe.add(new Telefonas.Builder().buildRandom());
//        aibe.add(new Telefonas.Builder().buildRandom());
//        aibe.add(new Telefonas.Builder().buildRandom());
//        
        Ks.oun("telefonu aibes vaizdavimas:");
        Ks.oun(aibe.toVisualizedString(""));
        Ks.oun(aibe.height());
        
        BstSetKTU<Telefonas> aibe2 = new BstSetKTU<Telefonas>();
        System.out.println("MEDZIO AUKSTIS");
        System.out.println(aibe.height());
        
        AvlSetKTU<Telefonas> aibe3 = new AvlSetKTU<>();
        
        Telefonas t1 = new Telefonas.Builder().buildRandom();
        Telefonas t2 = new Telefonas.Builder().buildRandom();
        Telefonas t3 = new Telefonas.Builder().buildRandom();
        Telefonas t4 = new Telefonas.Builder().buildRandom();
        Telefonas t5 = new Telefonas.Builder().buildRandom();
        Telefonas t6 = new Telefonas.Builder().buildRandom();
        Telefonas t7 = new Telefonas.Builder().buildRandom();
        Telefonas t8 = new Telefonas.Builder().buildRandom();
        
        aibe2.add(t4); aibe3.add(t4);
        aibe2.add(t2); aibe3.add(t2);
        aibe2.add(t3); aibe3.add(t3);
        aibe2.add(t1); aibe3.add(t1);
        aibe2.add(t5); aibe3.add(t5);
        aibe2.add(t6); aibe3.add(t6);
        aibe2.add(t7); aibe3.add(t7);
        
        Ks.oun("Bst telefonu aibes vaizdavimas:");
        Ks.oun(aibe2.toVisualizedString(""));
        Ks.oun("");
        
        Ks.oun("Bst.headset:");
        Ks.oun(aibe2.headSet(t4));
        Ks.oun("");
        
        Ks.oun("Bst.headset(2):");
        Ks.oun(aibe2.headSet(t4, true));
        Ks.oun("");
        
        Ks.oun("Bst.subset:");
        Ks.oun(aibe2.subSet(t2, t6));
        Ks.oun("");
        
        Ks.oun("Bst.subset(2):");
        Ks.oun(aibe2.subSet(t2, true, t6, true));
        Ks.oun("");
        
        Ks.oun("Bst.tailSet:");
        Ks.oun(aibe2.tailSet(t6));
        Ks.oun("");
        
        Ks.oun("Bst.floor:");
        Ks.oun(aibe2.floor(t8));
        Ks.oun("");
        
        Ks.oun(aibe2.toVisualizedString(""));
        Ks.oun(t3);
        Ks.oun("Bst.higher:");
        Ks.oun(aibe2.higher(t3));
        Ks.oun("");
        
        Ks.oun("Avl telefonu aibes vaizdavimas:");
        Ks.oun(aibe3.toVisualizedString(""));
        Ks.oun("");
        
        aibe3.remove(t2);
        
        Ks.oun("Avl aibe po remove:");
        Ks.oun(aibe3.toVisualizedString(""));
        Ks.oun("");       
    }
}
