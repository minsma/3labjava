package lab3smalinskas;

import laborai.gui.swing.Lab3Window;
import java.util.Locale;

public class VykdymoModulis {
    public static void main(String[] args) throws CloneNotSupportedException {
        Locale.setDefault(Locale.US); // Suvienodiname skaičių formatus
        TelefonuTestai.aibesTestas();
        Lab3Window.createAndShowGUI();
    }
}