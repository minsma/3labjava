package lab3smalinskas;

import laborai.studijosktu.KTUable;
import laborai.studijosktu.Ks;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;

public class Telefonas implements KTUable<Telefonas> {
    
    private static final int priimtinuIsleidimoMetuRiba = 2012;         // priimtinu telefono isleidimo metu riba
    private static final int esamiMetai = LocalDate.now().getYear();    // kintamasis, kuriame saugojama siu metu reiksme
    private static final double minKaina = 120.0;                       // minimali telefono kaina
    private static final double maxKaina = 1200.0;                      // maksimali telefono kaina
    private static final String idCode = "NA";                          // telefono id kodas
    private static int serNr = 100;                                     // telefono serijos numeris
    private final String prekesNumeris;                                 // telefono prekes numeris
    private String gamintojas = "";                                     // telefono gamintojas
    private String modelis = "";                                        // telefono modelis
    private int isleidMetai = -1;                                       // telefono isleidimo metai
    private double kaina = -1.0;                                        // telefono kaina
    
    public Telefonas(){
        prekesNumeris = idCode + (serNr++);                             // suteikiamas originalus prekes numeris
    }
    
    public Telefonas(String gamintojas, String modelis, int isleidMetai,
                     double kaina){
        prekesNumeris = idCode + (serNr++);
        this.gamintojas = gamintojas;
        this.modelis = modelis;
        this.isleidMetai = isleidMetai;
        this.kaina = kaina;
        validate();
    }
    
    public Telefonas(String dataString){
        prekesNumeris = idCode + (serNr++);
        this.parse(dataString);
    }
    
    public Telefonas(Builder builder){
        prekesNumeris = idCode + (serNr++);
        this.gamintojas = builder.gamintojas;
        this.modelis = builder.modelis;
        this.isleidMetai = builder.isleidMetai;
        this.kaina = builder.kaina;
        validate();
    }
    
    @Override 
    public Telefonas create(String dataString){
        return new Telefonas(dataString);
    }
    
    @Override
    public void parse(String dataString){
        try {
            Scanner scanner = new Scanner(dataString);
            this.gamintojas = scanner.next();
            this.modelis = scanner.next();
            this.isleidMetai = scanner.nextInt();
            this.kaina = scanner.nextDouble();
            validate();
        } catch (InputMismatchException e){
            Ks.ern("Blogas duomenu formatas apie telefona -> " + dataString);
        } catch (NoSuchElementException e){
            Ks.ern("Truksta duomenu apie telefona -> " + dataString);
        }
    }

    @Override
    public String validate()
    {
        String klaidosTipas = "";
        
        if(this.isleidMetai < priimtinuIsleidimoMetuRiba || this.isleidMetai > esamiMetai){
            klaidosTipas = "Netinkami isleidimo metai, turi buti [" + 
                    priimtinuIsleidimoMetuRiba + ":" + esamiMetai + "]";
        }
        
        if(kaina < minKaina || kaina > maxKaina){
            klaidosTipas += " Kaina uz leistinu ribu [" + minKaina + ":" +
                    maxKaina + "]";
        }
        
        return klaidosTipas;
    }

    @Override
    public String toString() {
        return "Telefonas{" + "prekesNumeris=" + prekesNumeris + 
                ", gamintojas=" + gamintojas + ", modelis=" + modelis + 
                ", isleidMetai=" + isleidMetai + ", kaina=" + kaina + '}';
    }

    public String getGamintojas() {
        return gamintojas;
    }

    public void setGamintojas(String gamintojas) {
        this.gamintojas = gamintojas;
    }

    public String getModelis() {
        return modelis;
    }

    public void setModelis(String modelis) {
        this.modelis = modelis;
    }

    public int getIsleidMetai() {
        return isleidMetai;
    }

    public void setIsleidMetai(int isleidMetai) {
        this.isleidMetai = isleidMetai;
    }

    public double getKaina() {
        return kaina;
    }

    public void setKaina(double kaina) {
        this.kaina = kaina;
    }

    public String getPrekesNumeris() {
        return prekesNumeris;
    }
    
    @Override
    public int compareTo(Telefonas telefonas) {
        return getPrekesNumeris().compareTo(telefonas.getPrekesNumeris());
    }
    
    public static Comparator<Telefonas> pagalIsleidMetus = 
            (Telefonas pirmas, Telefonas antras) -> 
                    Integer.compare(pirmas.getIsleidMetai(), antras.getIsleidMetai());
    
    public static Comparator<Telefonas> pagalKaina =
            (Telefonas pirmas, Telefonas antras) ->
                Double.compare(pirmas.getKaina(), antras.getKaina());
    
    public static Comparator<Telefonas> pagalGamintojaIrModeli =
            (Telefonas pirmas, Telefonas antras) -> {
                if(pirmas.getGamintojas().compareTo(antras.getGamintojas()) == 0)
                    return pirmas.getModelis().compareTo(antras.getModelis());
                
                return 0;
    };
    
    public static Comparator<Telefonas> pagalModeli =
            (Telefonas pirmas, Telefonas antras) ->
                    pirmas.getModelis().compareTo(antras.getModelis());
    
    public static class Builder {
        
        private final static Random RANDOM = new Random(1949);
        private final static String[][] MODELIAI = {
            {"Iphone", "4", "4s", "5", "5s", "6", "6s", "7", "8", "X"},
            {"LG", "G5", "G6"},
            {"Samsung Galaxy", "S8", "S8+", "NOTE 8", "S7", "S7 Edge", "A3", 
                "A5", "J3", "J3", "J5", "XCOVER 5"},
            {"Sony Xperia", "XZ1", "XZ Premium", "X Compact", "XA", "XZ", "XZS",
                "XA1", "L1", "E5", "M5"},
            {"Huawei", "P10", "P10 Lite", "P10 Plus", "Mate 9 Pro", 
                "P9 Lite 2017", "P9 Lite", "P9 Lite Mini", "Y6", "Nova"} 
        };
        
        private String gamintojas = "";
        private String modelis = "";
        private int isleidMetai = -1;
        private double kaina = -1.0;
        
        public Telefonas build(){
            return new Telefonas(this);
        }
        
        public Telefonas buildRandom() {
            int ma = RANDOM.nextInt(MODELIAI.length);               // gamintojas
            int mo = RANDOM.nextInt(MODELIAI[ma].length - 1) + 1;   // modelis
            
            return new Telefonas(
                    MODELIAI[ma][0],                               // gamintojas
                    MODELIAI[ma][mo],                              // modelis
                    2012 + RANDOM.nextInt(5),                     // metai tarp 2012 ir 2017
                    120.0 + RANDOM.nextDouble() * 1200.0           // kaina tarp 120 ir 1200
            );
        }
        
        public Builder gamintojas(String gamintojas){
            this.gamintojas = gamintojas;
            
            return this;
        }
        
        public Builder modelis(String modelis){
            this.modelis = modelis;
            
            return this;
        }
        
        public Builder isleidMetai(int isleidMetai){
            this.isleidMetai = isleidMetai;
            
            return this;
        }
        
        public Builder kaina(double kaina){
            this.kaina = kaina;
            
            return this;
        }
    }
}
