package lab3smalinskas;

import laborai.gui.MyException;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

public class TelefonuGamyba {
    private static Telefonas[] telefonai;
    private static int pradinisIndeksas = 0, galinisIndeksas = 0;
    private static boolean arPradzia = true;
    
    public static Telefonas[] generuoti(int kiekis){
        telefonai = new Telefonas[kiekis];
        
        for(int i = 0; i < kiekis; i++)
            telefonai[i] = new Telefonas.Builder().buildRandom();
        
        return telefonai;
    }
    
    public static Telefonas[] generuotiIrIsmaisyti(int aibesDydis,
            double isbarstymoKoeficientas) throws MyException {
        return generuotiIrIsmaisyti(aibesDydis, aibesDydis, isbarstymoKoeficientas);
    }
    
    public static Telefonas[] generuotiIrIsmaisyti(int aibesDydis, int aibesImtis,
            double isbarstymoKoeficientas) throws MyException {
        telefonai = generuoti(aibesDydis);
        return ismaisyti(telefonai, aibesImtis, isbarstymoKoeficientas);
    }
    
    public static Telefonas[] ismaisyti(Telefonas[] telefonuBaze,
            int kiekis, double isbarstKoef) throws MyException {
        if(telefonuBaze == null)
            throw new IllegalArgumentException("TelefonuBaze yra null");
        if(kiekis <= 0)
            throw new MyException(String.valueOf(kiekis), 1);
        if(telefonuBaze.length < kiekis)
            throw new MyException(telefonuBaze.length + " >= " + kiekis, 2);
        if((isbarstKoef <0) || (isbarstKoef > 1))
            throw new MyException(String.valueOf(isbarstKoef), 3);
        
        int likusiuKiekis = telefonuBaze.length - kiekis;
        int pradziosIndeksas = (int) (likusiuKiekis * isbarstKoef / 2);
        
        Telefonas[] pradineTelefonuImtis = Arrays.copyOfRange(telefonuBaze, 
                pradziosIndeksas, pradziosIndeksas + kiekis);
        Telefonas[] likusiTelefonuImtis = Stream
                .concat(Arrays.stream(Arrays.copyOfRange(telefonuBaze, 0, 
                                      pradziosIndeksas)),
                        Arrays.stream(Arrays.copyOfRange(telefonuBaze, 
                                      pradziosIndeksas + kiekis, 
                                      telefonuBaze.length)))
                .toArray(Telefonas[]::new);
        
        Collections.shuffle(Arrays.asList(pradineTelefonuImtis)
                .subList(0, (int) (pradineTelefonuImtis.length * isbarstKoef)));
        Collections.shuffle(Arrays.asList(likusiTelefonuImtis)
                .subList(0, (int)(likusiTelefonuImtis.length * isbarstKoef)));
        
        TelefonuGamyba.pradinisIndeksas = 0;
        galinisIndeksas = likusiTelefonuImtis.length - 1;
        TelefonuGamyba.telefonai = likusiTelefonuImtis;
        
        return pradineTelefonuImtis;
    }
    
    public static Telefonas gautiIsBazes() throws MyException {
        if((galinisIndeksas - pradinisIndeksas) < 0)
            throw new MyException(String.valueOf(galinisIndeksas - pradinisIndeksas), 4);
        
        arPradzia = !arPradzia;
        
        return telefonai[arPradzia ? pradinisIndeksas++ : galinisIndeksas--];
    }
}
