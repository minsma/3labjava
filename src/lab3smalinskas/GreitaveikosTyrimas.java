package lab3smalinskas;

import java.util.ArrayList;
import java.util.HashSet;
import laborai.studijosktu.AvlSetKTUx;
import laborai.studijosktu.SortedSetADTx;
import laborai.studijosktu.BstSetKTUx;
import laborai.gui.MyException;
import java.util.ResourceBundle;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;

public class GreitaveikosTyrimas {

    public static final String FINISH_COMMAND = "finish";
    private static final ResourceBundle MESSAGES = ResourceBundle.getBundle("laborai.gui.messages");

    private static final String[] TYRIMU_VARDAI = {"BstSort", "AvlSort", 
        "TreeSetAdd", "HashsetAdd", "TreeSetCAll", "HashSetCAll"};
    private static final int[] TIRIAMI_KIEKIAI = {10000, 20000, 50000, 80000, 100000};

    private final BlockingQueue resultsLogger = new SynchronousQueue();
    private final Semaphore semaphore = new Semaphore(-1);
    private final Timekeeper tk;
    private final String[] errors;

    private final SortedSetADTx<Telefonas> bstAibe = new BstSetKTUx(new Telefonas());
    private final SortedSetADTx<Telefonas> avlAibe = new AvlSetKTUx(new Telefonas());

    public GreitaveikosTyrimas() {
        semaphore.release();
        tk = new Timekeeper(TIRIAMI_KIEKIAI, resultsLogger, semaphore);
        errors = new String[]{
            MESSAGES.getString("error1"),
            MESSAGES.getString("error2"),
            MESSAGES.getString("error3"),
            MESSAGES.getString("error4")
        };
    }

    public void pradetiTyrima() {
        try {
            SisteminisTyrimas();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    public void SisteminisTyrimas() throws InterruptedException {
        try {
            
            ArrayList<Integer> skaiciai = new ArrayList<Integer>();
            skaiciai.add(5000);
            skaiciai.add(10000);
            skaiciai.add(8000);
            skaiciai.add(6000);
            skaiciai.add(5089);
            skaiciai.add(3215);
            
            for (int k : TIRIAMI_KIEKIAI) {                
                Telefonas[] telefonuMasyvas = TelefonuGamyba.generuotiIrIsmaisyti(k, 1.0);
                
                TreeSet<Integer> aibe = new TreeSet();
                HashSet<Integer> aibe2 = new HashSet();
                
                bstAibe.clear();
                avlAibe.clear();
                
                tk.startAfterPause();
                tk.start();
                
                for (Telefonas telefonas : telefonuMasyvas)
                    bstAibe.add(telefonas);
                
                
                tk.finish(TYRIMU_VARDAI[0]);
                
                for (Telefonas telefonas : telefonuMasyvas)
                    avlAibe.add(telefonas);
                
                tk.finish(TYRIMU_VARDAI[1]);
                
                for(int i = 0; i < k; i++)
                    aibe.add(i);
                
                tk.finish(TYRIMU_VARDAI[2]);
                
                for(int i = 0; i < k; i++)
                    aibe2.add(i);
                
                tk.finish(TYRIMU_VARDAI[3]);
                
                aibe.containsAll(skaiciai);
                
                tk.finish(TYRIMU_VARDAI[4]);
                
                aibe2.containsAll(skaiciai);
                
                tk.finish(TYRIMU_VARDAI[5]);
                
                tk.seriesFinish();
            }
            
            
            
            tk.logResult(FINISH_COMMAND);
        } catch (MyException e) {
            if (e.getCode() >= 0 && e.getCode() <= 3) {
                tk.logResult(errors[e.getCode()] + ": " + e.getMessage());
            } else if (e.getCode() == 4) {
                tk.logResult(MESSAGES.getString("msg3"));
            } else {
                tk.logResult(e.getMessage());
            }
        }
    }

    public BlockingQueue<String> getResultsLogger() {
        return resultsLogger;
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }
}