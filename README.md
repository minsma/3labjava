# 3 Duomenų Struktūrų su Java Labaratorinis darbas #

1.	Pakete lab3pavardė sudarykite individualių elementų aibės panaudojimo klasę, kurioje būtų elementų aibės formavimas, elemento priklausomumo aibei patikrinimas, aibės elemento šalinimas ir pan. (3 metodai). Sukurtų metodų veikimą demonstruokite pateiktuose Swing arba JavaFX dialoguose arba sukurkite nuosavą, pasinaudodami paskaitų medžiaga.  
2.	Sudarykite individualių elementų panaudojimo klasės testus su skirtingais duomenų rinkiniais.  
3.	Parašykite metodą, skaičiuojantį vieną pasirinktą medžio charakteristiką: medžio aukštį, paieškos kelio ilgį vidutiniu ir blogiausiu atvejais, papildymo, šalinimo ir kitų operacijų vykdymo laiko priklausomybę nuo medžio aukščio.   
4.	Atlikite klasių BstSetKTU ir AvlSetKTU metodų greitaveikos tyrimą ir rezultatus palyginkite. Sudarykite vykdymo laikų grafikus ir atlikite rezultatų analizę.   
5.	Klasėje BstSetKTU realizuokite metodus headSet(E e), subSet(E e1, E e2), tailSet(E e) ir iteratoriaus metodą remove(). Galite pasiūlyti ir realizuoti kitų prasmingų darbui su rikiuota aibe metodų, pvz. metodai, realizuojantys dviejų aibių sąjungą, aibių sankirtą, patikrinantys ar dvi aibės yra lygios.   
6.	Realizuokite klasės AvlSetKTU metodus: remove ir removeRecursive.   
7.	Realizuoti individualiai nurodytus metodus BstSetKTU klasėje.  
SortedSetADT<E> subSet(E fromElement, boolean fromInclusive, E toElement, boolean toInclusive)  
SortedSetADT<E> headSet(E toElement)  
E floor(E e)  
8.	Atliekamas individualiai nurodytų metodų greitaveikos tyrimas.  
TreeSet<Integer>	<->	HashSet<Integer>	metodas add(Object o)  
TreeSet<Integer>	<->	HashSet<Integer>	metodas containsAll(Collection<?> c)  
9.	Sunaudojamos atminties kiekio įvertinimas.
